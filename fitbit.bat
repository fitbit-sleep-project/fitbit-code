cd data
rmdir /S /Q raw
jupyter nbconvert --execute --to notebook --inplace "Extract Archive.ipynb"
cd ..
jupyter nbconvert --execute --to notebook --inplace "1. Data Engineering - Personal Time Series.ipynb"
jupyter nbconvert --execute --to notebook --inplace "2. Data Exploration - Personal Time Series.ipynb"
jupyter nbconvert --execute --to notebook --inplace "3. Prophet - Personal Time Series.ipynb"
jupyter nbconvert --execute --to notebook --inplace "4. PyCaret - Personal Time Series.ipynb"
jupyter nbconvert --execute --to notebook --inplace "5. Raw Models - Personal Time Series.ipynb"
del logs.log