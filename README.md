# Sleep Optimization with Fitness Tracker


This repository contains a study around my sleep problem (more precisely, confusion arousal dreams) that has started somewhere around May/April 2019. Naturally, when some problem becomes too annoying, one may want to apply some data science while looking for resolution.

Interestingly, many individuals are indeed concerned about their sleep, and this has likely motivated appearance of the industrial direction called Sleep Analytics. Sleep Analytics has become really popular nowadays, with typicaly business model based on sensor devices and analytics around measurements brought by such devices. An example of such products is the well-known High-End segment representative Eight Sleep Mattress Pod, explicitly recommended by famous business leaders including Mark Zuckerberg and Elon Musk.

With somewhat smaller budget, I have started with Fitbit. To study the problem rigorously, I bought a Fitbit fitness tracker that tracks sleep patterns in reasonable detalization. And after some data collection, there is a natural opportunity to apply data science with the purpose of sleep optimization.

Fitbit is the company I chose, although multiple competitors exist. Apple Watch, Google Watch, Oura Ring, Whoop 4.0, SleepOn Tracker, Withing Sleep, etc. Fitbit and various competitors do some data science as a part of their products, but I consider those an example for further improvement. 

Existing products are typically designed for helping humans without sleep problems, e.g. to boost productivity and improve life balance. In my case, however, the most interesting metrics are different, and therefore need to get engineered from scratch. Among those metrics are the number of times I wake up at night, time wasted awake between sleep segments, etc. Also, my questions are somewhat different from those brought by Fitbit. Will my time awake at night increase by an hour in the next two, ten, hundred years?How should I improve my sleep? Under what conditions may I sleep better? Also, a natural “sleep strong” bet is whether there exists a model predicting my next month's sleep better than average from the previous month (or daily average per weekday).


## Repository Overview

The notebook `data/Extract Archive.ipynb` extracts the Fitbit and Google Fit data. After the data is unarchived, the pipeline is the following:

1. The notebook `1. Data Engineering - Personal Time Series.ipynb` performs data engineering, builds features and creates dataset table for further processing.
2. The notebook `2. Data Exploration - Personal Time Series.ipynb` is designed to do data exploration. This notebook contains basic statistics alongside confidence intervals, correlations between time series, autocorrelations and decompositions (seasonal, STL).
3. The notebook `3. Prophet - Personal Time Series.ipynb` applies Facebook forecasting library Prophet to the interesting time series.
4. The notebook `4. PyCaret - Personal Time Series.ipynb` applies PyCaret AutoML to the interesting time series.
5. The notebook `5. Raw Models - Personal Time Series.ipynb` applies several models by explicitly learning and building them from scratch. 


The file `sleep-with-fitness-tracker.pdf` contains the presentation describing the motivation, process and findings.
